![Build Status](https://gitlab.com/pages/hexo/badges/master/build.svg)

README.md
=========

###TO DO:

 - [ ] About Page
 - [ ] Project page
 - [ ] Slide page
 - [ ] A slide plug-in
 - [X] Dotted date format
 - [ ] Other icons to find me
 - [ ] Build my own theme
 - [ ] Fix Author name in Uppercase (do like this: Djanilson Alves)

