---
title: Safe way clean '/boot' partition ubuntu
tags: ubuntu
dest: _posts/ubuntu
date: 2020/4/5 20:46:25
---

## 0. The planned way

_* I'll explan pass by pass in the next session_

1. Check the current kernel version.
2. Remove the OLD kernels
3. Remove ever packages you won't need anymore
4. Update grub

## 1. Check the current kernel version

```bash
$ uname -r
```

## 2. List and remove the OLD kernels

To list use this:
```bash
$ sudo dpkg --list 'linux-image*'|awk '{ if ($1=="ii") print $2}'|grep -v `uname -r`
```

 and you will recive a list:
```bash
linux-image-3.19.0-25-generic
linux-image-3.19.0-56-generic
linux-image-3.19.0-58-generic
linux-image-3.19.0-59-generic
linux-image-3.19.0-61-generic
```

now, remove then all:
```bash
$ sudo apt-get purge linux-image-3.19.0-25-generic
$ sudo apt-get purge linux-image-3.19.0-56-generic
$ sudo apt-get purge linux-image-3.19.0-58-generic
$ sudo apt-get purge linux-image-3.19.0-59-generic
$ sudo apt-get purge linux-image-3.19.0-61-generic
$ sudo apt-get purge linux-image-3.19.0-65-generic
```

_** I think that the code below make it in one line, but i dont try yet_

```bash
$ sudo dpkg --list 'linux-image*'|awk '{ if ($1=="ii") print $2}'|grep -v uname -r | while read -r line; do sudo apt-get -y purge $line;done;
```

## 3. Remove ever packages you won't need anymore

the cleaner not end yet :smile:


```bash
$ sudo apt-get autoremove && $ sudo apt-get autoclean
```

and clean `Snap` cache
_* This likely requires bash or a compatible shell with the [[ construct._

```bash
$ snap list --all | while read snapname ver rev trk pub notes; do if [[ $notes = *disabled* ]]; then snap remove "$snapname" --revision="$rev"; fi; done
```

## 4. Update grub
And finish this job udating the Grub Kernel List

```bash
$ sudo update-grub
```