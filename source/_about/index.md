---
title: about
date: 2018-09-05 22:17:39
---

# # Hello, my name is Djanilson Alves and I'm a programmer! 

## Quick Review about me
 - Bacharelando em Sistemas de Informação, com previsão de término para 2018; 
 - Programador Javascript há seis anos; 
 - Intermediário em Inglês e Novato em Japonês; 
 - Hobbista de fotografia; 
 - Amante de cafés especiais <small>(a ponto de ter um curso de barista pelo [Sindicafé](http://www.sindicafesp.com.br))</small>; 
 - Contribuinte do [0xe Hacker Club](https://www.facebook.com/0xehackerclub/)

## Experiências Profissionais

### 2018:
- **Audora**
    **Cargo**: Programador Pleno

### 2017:
- **CTIS-Sonda**
    **Cargo**: Programador Pleno

### 2016:
- **CTIS-Sonda**
    **Cargo**: Programador Pleno

### 2015:
- **Audora**
    **Cargo**: Estagiário

### 2014:
 - **Reitoria do Instituto Federal de Educação - Bolsista**
    **Setor:** Pró-Reitoria de Pesquisa e Inovação
    **Cargo**: Estagiário
    
 - **Pragmatics**
    **Cargo**: Estagiário

### 2013:
 - **Abril - SENAI/AL**
    Programador - Estágiario

 - **Janeiro - SENAI/AL**
    Competidor na Olimpíada do Conhecimento SENAI/AL;

## Linha do tempo em detalhes
<small>_Versão descrita da minha jornada como programador_</small>

**SENAI/AL - 2013**
Inicialmente fui competidor, na etapa estadual, da Olimpíada do Conhecimento concorrendo com outros competidores. Não passando para a etapa nacional, mas o SENAI me contratou para um estágio onde construí um sistema de manutenções preventivas e corretivas denominado SIGEM.
Na ocasião trabalhei com `JQuery`, `HTML`, `CSS` para front-end e `PHP-OO`, `MySQL` para back-end onde foi implementado uma `API Rest`. Também foi criado um aplicativo utilizando `Phonegap Cordova` que consumia a `API Rest` para exibir as Ordens de Serviços para os responsáveis por realizar as manutenções.
Ao fim de um ano de estágio tomei a decisão de sair do SENAI, visando pegar projetos mais desafiadores e assim crescer mais dentro da área.

**Pragmatics - 2014**
Saindo do SENAI entrei na Pragmatics, a empresa de um professor da faculdade, na mesma época em que `ReactJS` começou a ser divulgado e se popularizar. Fui chamado para migrar algumas aplicações de `EmberJS` para `ReactJS`, foi onde aprendi a utilizar Javacsript de verdade (não mais `JQuery`), bem como, `SASS`, `NodeJS`, `Gulp`, `Webpack`, `NPM`. Depois de algum tempo, recebi, do mesmo professor a proposta de ingressar, como bolsista, em uma equipe que desenvolveria um sistema acadêmico para submissão de artigos científicos, esse sistema era para faculdade da onde eu estudava, eu aceitei.

**Reitoria IFAL - 2014**
Ingressei no projeto do Sistema Comunica, um sistema, como disse anteriormente, voltado para submissão de artigos científicos, atuei como front-end e designer de interfaces, as vezes tutoreando os novos integrantes que ingressavam, no total foram mais de 20 bolsistas entre back-end e front-end que fizeram parte do projeto. Enquanto era bolsista, concorri a um estágio na empresa Audora, que ficava um quarteirão depois da Reitoria, acabei sendo contratado como estágiario. Na ocasião utilizamos a mesma tecnologia que a Pragmatics utilizava (`ReactJS`, `SASS`, `Webpack`, `NPM`, `NodeJS`) porém o banco de dados passou a ser o `MongoDB` que é um banco de dados `NoSQL`.

**Audora - 2015**
A Audora trabalha com um sistema enorme, seu forte é processos eletrônicos, mas há outros módulos que compõem o sistema. Atuei na construção de novos módulos, bem como, devido a minha experiência com `JQuery`, a manutenção e migração de uma parte do sistema construída em `JQuery` nos anos iniciais da empresa. Os novos módulos eram criados em `ReactJS`, integrados com um `framework Java` construído pelos fundadores da empresa quando esta estava nascendo.

**CTIS Tecnologia - 2016**
Fui recomendado pela Audora para a empresa CTIS Tecnologia que precisava contratar, com uma certa urgência, programadores para a construção de um sistema para a Previdência Social, sem dúvida aceitei essa mudança. A construção do sistema de certo modo foi desafiador, o que me levou a mudar de residência e passar de 20km de distância para 3km de casa para o trabalho. A equipe era excelente de se trabalhar, o gestor era bem organizado, nessa ocasião pude aprender algumas coisas sobre `SCRUM` e Gestão de Projetos. Quanto as tecnologias utilizadas não mudou muito, visto que pudemos sugerir e a Audora também indicou utilizar `ReactJS`. Após um tempo o projeto foi concluído e surgiu a oportunidade de voltar para a Audora, visto que pela própria visão operacional da CTIS Tecnologia, que não é uma empresa do estado em que resido, ao concluir um projeto verifica-se a necessidade de manter os desenvolvedores, caso não seja necessário há um desligamento. Com isso fui encaminhado a Audora, não só eu como outros desenvolvedores e analistas da CTIS Tecnologia qeu fizeram parte.

**De volta a Audora - 2018**
Agora não mais como estagiário, mas como programador pleno, voltei à Audora que havia crescido, captado mais clientes dentro do e fora do estado. Com esse fluxo coisas novas e complexas foram criadas, o sistema cresceu bastante e a equipe também. Sempre nos mantínhamos atualizados às novidades tecnológicas que surgiam de modo que testamos, adotamos tecnologias e ferramentas como `ES6`, `babel`, `ReactNative`, `Redux`, `Redux-Saga`, `Redux-Form`, `Yarn`… entre outras. É onde estou atualmente, a equipe é muito legal e os desafios são diários, mais o café ajuda nessas horas!